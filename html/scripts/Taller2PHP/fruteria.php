<?php
    /*  Una frutería ofrece las manzanas con descuento según la siguiente tabla:
        KILOS       %DESCUENTO
        0-2         0%
        2.01-5      10%
        5.01-10     20%
        Mayor a 10  30%
        Determine cuanto pagara una persona que compra manzanas en esa frutería.*/
    
    //variables

    $k=0; $result="";

    if(isset($_POST["btncalcular"])){
        
        //entrada
        $k=(float)$_POST["txtk"];   

        //proceso
        
        if($k==0.0 or $k<=2.0){
            $result="0% de descuento";
        }else if($k>2.0 and $k<=5.0){
            $result="10% de descuento";
        }else if($k>5.0 and $k<=10.0){
            $result= "20% de descuento";
        }else if($k>10.0){
            $result= "30% de descuento";
        }
         
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Frutería</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
<form method="post" action="fruteria.php">
        <div>
            <h1>Descuento por kilos</h1>
        </div>
        <div>
        <label for="dado1">cuantos kilos: </label>
        <input type="number" step="0.01" id="txtk" name="txtk" value="<?=$k?>"><br>
        
        
        <label for="ni">total descuento: </label>
        <input type="txt" id="txtr" name="txtr" value="<?=$result?>"><br>
               
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>