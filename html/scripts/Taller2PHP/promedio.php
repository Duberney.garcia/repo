<?php
    /*  Después de ingresar 4 notas, obtener el promedio de las tres mejores y 
        mostrar el mensaje “aprobado”, si el promedio es mayor o igual a 11; 
        caso contrario mostrar “desaprobado” */
    
    //variables
    $nota1=0; $nota2=0;$nota3=0;$nota4=0; $result="";
    $prom=0;
    if(isset($_POST["btncalcular"])){
        
        //entrada
        $nota1=(int)$_POST["txtn1"];
        $nota2=(int)$_POST["txtn2"];
        $nota3=(int)$_POST["txtn3"];
        $nota4=(int)$_POST["txtn4"];
        //proceso
        if ($nota1 <= $nota2 and $nota1 <= $nota3 and $nota1 <= $nota4) {
            $prom = $nota2 + $nota3 + $nota4 / 3;
        } else {
            if ($nota2 <= $nota1 and $nota2 <= $nota3 and $nota2 <= $nota4) {
                $prom = $nota1 + $nota3 + $nota4 / 3;
            } else {
                if ($nota3 <= $nota1 and $nota3 <= $nota2 and $nota3 <= $nota4) {
                    $prom = $nota1 + $nota2 + $nota4 / 3;
                } else {
                    $prom = $nota1 + $nota2 + $nota3 / 3;
                }
            }
        }
        if ($prom >= 11) {
            $result = "APROBADO";
        } else {
            $result = "DESAPROBADO";
        }
        
         
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DADOS</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
<form method="post" action="promedio.php">
        <div>
            <h1>Promedio</h1>
        </div>
        <div>
        <label for="nota1">Nota 1:</label>
        <input type="number" id="txtn1" name="txtn1" value="<?=$nota1?>"><br>
        <label for="dado2">Nota 2:</label>
        <input type="number" id="txtn2" name="txtn2" value="<?=$nota2?>"><br>
        <label for="dado3">Nota 3:</label>
        <input type="number" id="txtn3" name="txtn3" value="<?=$nota3?>"><br>
        <label for="dado3">Nota 4:</label>
        <input type="number" id="txtn4" name="txtn4" value="<?=$nota4?>"><br>

        <label for="ni">Resultado promedio: </label>
        <input type="txt" id="txtr" name="txtr" value="<?=$result?>"><br>
               
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>