<?php
    /*  Una frutería ofrece las manzanas con descuento según la siguiente tabla:
        KILOS       %DESCUENTO
        0-2         0%
        2.01-5      10%
        5.01-10     20%
        Mayor a 10  30%
        Determine cuanto pagara una persona que compra manzanas en esa frutería.*/
    
    //variables
    $n=0; $result=0;

    if(isset($_POST["btncalcular"])){
        
        //entrada
        $n=(int)$_POST["txtn"];   

        //proceso
        while ($n>=1) {
            $n=$n/10;
            $result ++;    
        }   
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Frutería</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
<form method="post" action="Ndigitos.php">
        <div>
            <h1>Numero de digitos</h1>
        </div>
        <div>
        <label for="numero">Numero </label>
        
        <input type="number" id="txtn" name="txtn" value="<?=$n?>"><br>
        
        <label for="n">el numero ingresado tiene <input type="txt" id="txtr" name="txtr" value="<?=$result?>"> digitos</label><br>
        
              
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>