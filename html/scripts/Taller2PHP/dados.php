<?php
    /*  Diseñe un algoritmo que califique el puntaje obtenido en el lanzamiento de 
        tres
        dados en base a la cantidad de seis obtenidos, de acuerdo a lo siguiente.
        • Tres seis: ORO
        • Dos seis: PLATA
        • Un seis: Bronce
        • Ningún seis: Perdió
        Análisis:
        Para la solución de este problema se requiere que el usuario ingrese el 
        puntaje de los dados, luego que el sistema verifique y determine el premio.*/
    
    //variables
    $dado1=0; $dado2=0; $dado3=0; $result="";

    if(isset($_POST["btncalcular"])){
        
        //entrada
        $dado1=(int)$_POST["txtd1"];
        $dado2=(int)$_POST["txtd2"];
        $dado3=(int)$_POST["txtd3"];

        //proceso
        
        if ($dado1 + $dado2 + $dado3==18) {
            $result='ORO';
        }elseif($dado1 + $dado2==12 or $dado2 + $dado3==12 or $dado1 + $dado3==12){
            $result="PLATA";
        }elseif($dado1==6 or $dado2==6 or $dado3==6){
            $result="BRONCE";
        }else{
            $result="Perdió";
        }
         
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DADOS</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
<form method="post" action="dados.php">
        <div>
            <h1>Resultado Dados</h1>
        </div>
        <div>
        <label for="dado1">Dado 1:</label>
        <input type="number" id="txtd1" name="txtd1" value="<?=$dado1?>"><br>
        <label for="dado2">Dado 2:</label>
        <input type="number" id="txtd2" name="txtd2" value="<?=$dado2?>"><br>
        <label for="dado3">Dado 3:</label>
        <input type="number" id="txtd3" name="txtd3" value="<?=$dado3?>"><br>
        
        <label for="ni">Resultado</label>
        <input type="txt" id="txtr" name="txtr" value="<?=$result?>"><br>
               
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>