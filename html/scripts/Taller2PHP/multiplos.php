<?php
    /*  Hallar cuantos múltiplos de M hay en un rango de números enteros.*/
    
    //variables
    $m=0; $n=0;  $result=0;

    if(isset($_POST["btncalcular"])){
        
        //entrada   
        $n=(int)$_POST["txtn"];
        $limit=(int)$_POST["txtlm"];

        //proceso
        while($m++ <=$limit){
            if($m%$n==0){
               $result= "$m Es multiplo $n";
            }
        }
       
        /*for($i=0;$i<$limit;$i++){
            if($i%$n==0){
            echo $i." es múltiplo de ".$n;
            }
        }*/ 
    }
   
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Multiplos</title>
    
</head>
<body>
<form method="post" action="multiplos.php">
        <div>
            <h1>Resultado de multiplos</h1>
        </div>
        <div>
        <label for="numero">Numero: </label>
        <input type="number" id="txtn" name="txtn" value="<?=$n?>"><br>
        <label for="limit">limite: </label>
        <input type="number" id="txtlm" name="txtlm" value="<?=$limit?>"><br>        
        
        <input type="txt" id="txtr" name="txtr" value="<?=$result?>"><br>        
              
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>