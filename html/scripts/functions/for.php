<?php
    /* Cada vez que requiere repetir un proceso una determinada 
    cantidad de veces, debera usar la estructura repetiva "FOR" que permitira 
     realizar en formas simple de este trabajo.
     Esta estructura utiliza una variable "Contador"
     donde se establece el valor inicial (VI),
     valor y el incremento (inc), que determina las veces a repetir la instruccion.*/
    
    /*Dado un rango de numeros entero, obtener la cantidad
    de numeros enteros que contiene*/

    //variables
    $i=0; $ni=0; $nf=0; $c=0; $nii=0; $nff=0;

    if(isset($_POST["btncalcular"])){
        //entrada
        $ni=(int)$_POST["txtni"];
        $nf=(int)$_POST["txtnf"];

        //proceso
        $nii=$ni;
        $nff=$nf;
        $ni=$ni+1;
        $nf=$nf-1;
        for ($i=$ni; $i<= $nf ;$i++ ) { 
            $c+=1;
            $ni=$nii;
            $nf=$nff;
        }    
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Usando el for</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="for.php">
        <div>
            <h1>Utilizando el FOR</h1>
        </div>
        <div id="container">
        <label for="ni">Numero inicial</label><span>
        <input type="txt" id="txtni" name="txtni" value="<?=$ni?>"></span>
        <br>
        <label for="nf" id="nf" name="nf">Numero final</label>
        <input type="txt" name="txtnf" id="txtnf" value="<?=$nf?>">
        <br>
        <label for="cantidad" name="cantidad" id="cantidad">Cantidad</label>
        <input type="text" name="txtc" id="txtc" value="<?=$c?>">
        <br>
        &nbsp;
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>