<?php
    date_default_timezone_set ("UTC");

    $meses=[1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",
    6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",
    11=>"Noviembre",12=>"Diciembre"];

    echo "Dia del año" date("z");
    echo "Estamos en el mes".$meses[date("z")];
    
    // esta funcion devuelve un numero aleatorio
    $numero_aletorio = rand(100,1000);
    echo $numero_aletorio;
    
    //esta funcion permite ver la fecha actual
    echo "Hoy es: ".date("d-m-a");
?>