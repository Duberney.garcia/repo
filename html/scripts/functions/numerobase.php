<?php
/*Dado un numero entero a base 10, convertir el numero a otra base menor que 10 */

//Variables
$n=0; $b=0; $d=0; $i=0; $r=0; $nn=0;

    //Condicional
    if(isset($_POST["btncalcular"])){
        //Entrada
        $n= (int)$_POST["txtn"];
        $b= (int)$_POST["txtb"];
        //Proceso
        $nn=$n;
        while($n>0){
            $d=$n%$b;
            $n= (int) ($n/$b);
            $i= $i * 10 + $d;
        }
        while($i>0){
            $d=$i%10;
            $i= (int) ($i/10);
            $r= $r*10+$d;
        }
        $n=$nn;
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Numero base 10</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="numerobase.php">
        <div>
            <h1>Convertir numero a otra base</h1>
        </div>
        <div id="container">
        <label for="numero">Numero base 10</label><span>
        <input type="number" id="txtn" name="txtn" palceholder="numero base 10" value="<?=$n?>"></span>
        <br>
        <label for="convertir" id="convertir" name="convertirn">numero a convertir</label>
        <input type="number" name="txtb" id="txtb" value="<?=$b?>">
        <br>
        <label for="result" name="txtr" id="txtr">resultado</label>
        <input type="text" name="result" id="result" value="<?=$r?>">
        <br>
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>