<?php
    /* Determinar cuantos numeros primos hay en los primeros N numero 
    enteros positivos*/

    $n=0; $c=0; $i=0; $j=0; $flag=false;
    if(isset($_POST["btncalcular"])){
        //entrada
        $n=(int)$_POST["txtn"];
      
        //proceso
        for ($i=2; $i <=$n ; $i++) { 
            $flag=true;
            for ($j=2;  $j<=$i/2 ; $j++) { 
               if($i % $j==0){
                   $flag=false;
                   break;
               }
            }
            if($flag){
                $c+=1;
                $flag=true;
            }
            
        }
    }
    
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Numero base 10</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="numerosprimos.php">
        <div>
            <h1>Numeros primos con php</h1>
        </div>
        <div id="container">
        <label for="num">Numero</label>
        <input type="number" id="txtn" name="txtn" value="<?=$n?>" required>
        <br> 
        <label for="cantidad" name="cantidad" id="cantidad">Cantidad</label>
        <input type="text" name="txtc" id="txtc" value="<?=$c?>">
        <br>
        
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>