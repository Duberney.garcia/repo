<?php
    /* Hacer un programa en php que tenga un aaray de 8 numeros enteros
    que haga lo siguiente 
    -recorrerlo y mostralos
    -ordenado y mostrarlo
    - mostrar su longitud
    -buscar algun elemento dentro de el*/

    //funciones
    function mostraArray($numeros){
        $resultado="";
        foreach($numeros as $numero){
            $resultado .= $numero. "<br>";     
        }
        return $resultado;
    }
    $numeros=array(5,10,45,50,85,94,60,7);

    //recorrer y mostrar 
    foreach($numeros as $numero){
        echo"<br><h3>El valor del array es : </h3>".$numero;
    }

    //ordenarlo y mostrarlo
    /*  Utilizamos la funcion sort() esta funcion ordena un array
        Los elementos estaran ordenados de menor a mayor cuando la 
        fucion haya terminado */
    echo"<br><h1>El valor del array es : </h1>";
    sort($numeros, SORT_NUMERIC);
    echo mostraArray($numeros);
    
    //Busqueda en el array
    if(isset($_GET['numero'])){
        $busqueda = $_GET['numero'];
        echo"<h1>Buscar en el array el numero $busqueda </h1>";
        $search = array_search($busqueda,$numeros);
        if(!empty($search)){
            echo"<h4>El numero buscado existe en el array, en el indice: $search</h4>";
        }else{
            echo"<h4>El numero buscado  NO existe en el array</h4>";  
        }
    }
?>