<?php
    /* 
        Hacer un programa en php que compruebe si una variable
        esta vacia y si lo esta rellenarla con texto en minusculas i
        en mayusculas y negrita.
    */

    $texto ="Soy aprendiz del ADSI";
    if(empty($texto)){
        /*Devuelve el string con todos los caracteres alfabéticos convertidos a 
        mayúsculas.
        Notar que ser 'alfabético' está determinado por la configuración regional
        actual. 
        Por ejemplo, en la configuración regional por defecto "C" caracteres 
        como la diéresis-a (ä) no se convertirán. */
        $texto ="con este texto relleno la variable texto";
        $textomayus = strtoupper($texto);
        echo"<strong>$textomayus</strong>";
    }else{
        $textomayus = strtoupper($texto);
        echo"la variable tiene este contenido dentro "."<mark>$textomayus</mark>";
    }
?>