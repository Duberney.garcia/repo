<?php
   
   $producto = $_POST["producto"];
   $precio = $_POST["precio"];
   $cantidad =$_POST["cantidad"];
   $totalp=$precio*$cantidad;

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset=utf-8>
        <title>Introduccion a PHP</title>
        <link rel="stylesheet" href="estilos.css">
    </head>
    <body>
        <h1>Total a pagar</h1>
        <div>
            <p>Producto: <?php echo $producto?></p>
            <p>Precio: <?php echo $precio?></p>
            <p>Cantidad: <?php echo $cantidad?></p>
            <p>Total a pagar: <?php echo $totalp?> </p>             
        </div> 
        <div>
            <a href="calcularpedido.html">Volver atras</a>
        </div>     
    </body>
</html>