<?php
    /*Busque un numero dentro de una matriz 4*3 y determine la posicion, y si existe o no 
      el numero buscado. use el metodo de busqueda secuencial */
    //Variables
    $nb=0; $i=0; $j=0; $p1=0; $p2=0; $r="";
    //Arreglo 
    $n=array();
    if(isset($_POST['btncalcular'])){
        //Entrada
        $n[0][0]=$_POST['txtn00'];
        $n[0][1]=$_POST['txtn01'];
        $n[0][2]=$_POST['txtn10'];
        $n[1][0]=$_POST['txtn11'];
        $n[1][1]=$_POST['txtn20'];
        $n[1][2]=$_POST['txtn21'];
        $n[2][0]=$_POST['txtn30'];
        $n[2][1]=$_POST['txtn31'];
        $n[2][2]=$_POST['txtn40'];
        $n[3][0]=$_POST['txtn41'];
        $n[3][1]=$_POST['txtn50'];
        $n[3][2]=$_POST['txtn51'];
        $nb =$_POST["txtnb"];

        //Proceso 
        $r="No Existe";
        $p1 =-1;
        $p2=-1;

        for ($i=0; $i <=3 ; $i++) { 
            for ($j=0; $j <=2 ; $j++) { 
                if($n[$i][$j]==$nb){
                    $r="Si Existe";
                    $p1=$i;
                    $p2=$j;
                }
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Busqueda secuencial</title>
    <link rel="stylesheet" href="stilos.css">
</head>
<body>
    <form action="secuencial.php" method="post">
        <h1>Sumar matriz 4*3</h1>
        <label for="number1">Numero 1</label>
        <input type="number" name="txtn00" id="txtn00" value="<?=$n[0][0]?>" size ="10"><hr>
        <label for="number4">Numero 2</label>
        <input type="number" name="txtn01" id="txtn01" value="<?=$n[0][1]?>" size ="10"><hr>
        <label for="number5">Numero 3</label>
        <input type="number" name="txtn10" id="txtn10" value="<?=$n[0][2]?>" size ="10"><hr>
        <label for="number5">Numero 4</label>
        <input type="number" name="txtn11" id="txtn11" value="<?=$n[0][3]?>" size ="10"><hr>

        <label for="number2">Numero 5</label>
        <input type="number" name="txtn20" id="txtn20" value="<?=$n[1][0]?>" size ="10"><hr>
        <label for="number5">Numero 6</label>
        <input type="number" name="txtn21" id="txtn21" value="<?=$n[1][1]?>" size ="10"><hr>
        <label for="number5">Numero 7</label>
        <input type="number" name="txtn30" id="txtn30" value="<?=$n[1][2]?>" size ="10"><hr>
        <label for="number5">Numero 8</label>
        <input type="number" name="txtn31" id="txtn31" value="<?=$n[1][3]?>" size ="10"><hr>

        <label for="number3">Numero 9</label>
        <input type="number" name="txtn40" id="txtn40" value="<?=$n[2][0]?>" size ="10"><hr>
        <label for="number5">Numero 10</label>
        <input type="number" name="txtn41" id="txtn41" value="<?=$n[2][1]?>" size ="10"><hr>     
        <label for="number5">Numero 11</label>
        <input type="number" name="txtn50" id="txtn50" value="<?=$n[2][2]?>" size ="10"><hr>               
        <label for="number5">Numero 12</label>
        <input type="number" name="txtn51" id="txtn51" value="<?=$n[2][3]?>" size ="10"><hr>

        <label for="suma">Numero a buscar</label>
        <input type="text" name="txtnb" id="txtnb" value="<?=$nb?>"  size="10">
        <label for="suma">Respuesta</label>
        <input type="text" name="txts0" id="txts0" value="<?=$r?>"  size="10">
        <label for="suma">Posicion 1</label>
        <input type="text" name="txts1" id="txts1" value="<?=$p1?>"  size="10">
        <label for="suma">Posicion 2</label>
        <input type="text" name="txts2" id="txts2" value="<?=$p2?>"  size="10">
        &nbsp;
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">

    </form>
</body>
</html>