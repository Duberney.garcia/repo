<?php
    /*Ingrese 6 numeros en un arreglo de dos dimensiones (matriz)3*2 y obtenga la 
    la suma de los numeros ingresados*/
    //variables 
    $suma=0;
    //Array
    $n=array();
    if(isset($_POST['btncalcular'])){
        $n[0][0]=$_POST['txtn00'];
        $n[0][1]=$_POST['txtn01'];
        $n[1][0]=$_POST['txtn10'];
        $n[1][1]=$_POST['txtn11'];
        $n[2][0]=$_POST['txtn20'];
        $n[2][1]=$_POST['txtn21'];

        //proceso
        for ($i=0; $i <=2 ; $i++) { 
            for ($j=0; $j <=1 ; $j++) { 
                $suma += $n[$i][$j];                
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Problema suma 5 numeros</title>
    <link rel="stylesheet" href="stilos.css">
</head>
<body>
    <form action="sumaarray.php" method="post">
        <h1>Arreglos 3*2</h1>
        <label for="number1">Numero 1</label>
        <input type="number" name="txtn00" id="txtn00" value="<?=$n[0][0]?>" size ="10"><hr>
        <label for="number2">Numero 2</label>
        <input type="number" name="txtn01" id="txtn01" value="<?=$n[0][1]?>" size ="10"><hr>
        <label for="number3">Numero 3</label>
        <input type="number" name="txtn10" id="txtn10" value="<?=$n[1][0]?>" size ="10"><hr>
        <label for="number4">Numero 4</label>
        <input type="number" name="txtn11" id="txtn11" value="<?=$n[1][1]?>" size ="10"><hr>
        <label for="number5">Numero 5</label>
        <input type="number" name="txtn20" id="txtn20" value="<?=$n[2][0]?>" size ="10"><hr>
        <label for="number5">Numero 6</label>
        <input type="number" name="txtn21" id="txtn21" value="<?=$n[2][1]?>" size ="10"><hr>

        <label for="suma">Suma</label>
        <input type="text" name="txts" id="txts" value="<?=$suma?>"  size="10">
        &nbsp;
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">

    </form>
</body>
</html>