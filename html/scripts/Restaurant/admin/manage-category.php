<?php include ('partials/menu.php');?>

<div class="main-content">
    <div class="wrapper">
        <h1>Manage Category</h1><br><br>

        <!-- Button to add admin -->
        <a href="#" class="btn-primary">Add Category</a><br><br><br>
        <table class="tbl-full"> 
            <tr>
                <th>S.N</th>
                <th>FULL NAME</th>
                <th>USERNAME</th>
                <th>ACTIONS</th>
            </tr>
            <tr>
                <td>1.</td>
                <td>Duberney garcia</td>
                <td>Zarcher</td>
                <td>
                    <a href="#" class="btn-secondary">Update Admin</a>
                    <a href="#" class="btn-danger">Delete Admin</a>
                </td>
            </tr>
            <tr>
                <td>2.</td>
                <td>Sebastian agudelo</td>
                <td>Segado</td>
                <td>
                    <a href="#" class="btn-secondary">Update Admin</a>
                    <a href="#" class="btn-danger">Delete Admin</a>
                </td>
            </tr>
            <tr>
                <td>3.</td>
                <td>Juan felipe G</td>
                <td>Goma</td>
                <td>
                    <a href="#" class="btn-secondary">Update Admin</a>
                    <a href="#" class="btn-danger">Delete Admin</a>
                </td>
            </tr>
            <tr>
                <td>4.</td>
                <td>yeraldine lara</td>
                <td>GuerriYeral</td>
                <td>
                    <a href="#" class="btn-secondary">Update Admin</a>
                    <a href="#" class="btn-danger">Delete Admin</a>
                </td>
            </tr>
        </table>
    </div>
</div>

<?php include('partials/footer.php');?>