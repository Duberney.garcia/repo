<!-- la pizzeria 'coma rico', ofrece la siguiente carta:
pizza de jamon y queso: 28.000
pizza hawuaiana: 30.000
pizza pepperoni: 30.000
pizza queso y pepperoni: 45.000

hay que tener presente que la pizzeria tiene varios gastos como:
merienda de sus empleados
pago de una deuda diaria por parta del dueño: 15.000

su labor sera construir un programa que permita
saber la venta diaria
conocer cual fue el valor vendido por cada clase de pizza
cuanto le queda a la pizzeria diario.
-->

<?php

//variables

    $pjq=0; $ph=0; $pp=0;
    $pqp=0; $total=0;$gastos=0;
    $deuda=15000;
    $vendia=0;
    $vpjq=0;
    $vph=0;
    $vpp=0;
    $vpqp=0;

    if(isset($_POST["btncalcular"])){
        
        //entrada
        $pjq=(double)$_POST["txtpjq"]; 
        $ph=(double)$_POST["txtph"]; 
        $pp=(double)$_POST["txtpp"]; 
        $pqp=(double)$_POST["txtpqp"];   
        $gastos=(double)$_POST["txtg"];

        //proceso
        $vpjq=$pjq*28000;
        $vph=$ph*30000;
        $vpp=$pp*30000;
        $vpqp=$pqp*45000;



        $total=($vpjq + $vph + $vpp+ $vpqp) - ($gastos + $deuda);
        
        
       
         
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pizzeria panuchi</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
<form method="post" action="pizzeria.php">
        <div>
            <h1>venta del dia</h1>
        </div>
        <div>
        <label for="pizza"> Tipo Pizza :</label>    
        <select name="txtp" id="txtp">
              <option value="-">-</option>  
              <option value="jamon y queso">jamon y queso</option>          
              <option value="hawaina">hawaina</option>          
              <option value="Napolitana">Napolitana</option>          
              <option value="queso y pepperoni">queso y pepperoni</option>          
        </select><br><br>   
        <label for="pizza">Pizza jamon y queso: </label>
        <input type="number" id="txtpjq" name="txtpjq" value="<?=$pjq?>"><br>

        <label for="pizza">Pizza Hawuaiana: </label>
        <input type="number" id="txtph" name="txtph" value="<?=$ph?>"><br>

        <label for="pizza">Pizza Pepperoni: </label>
        <input type="number" id="txtpp" name="txtpp" value="<?=$pp?>"><br>

        <label for="pizza">Pizza Queso y Pepperoni: </label>
        <input type="number" id="txtpqp" name="txtpqp" value="<?=$pqp?>"><br>

        <label for="ni">Gastos</label>
        <input type="txt" id="txtg" name="txtg" value="<?=$gastos?>"><br>

        <label for="ni">total pizza jamon y queso</label>
        <input type="txt" id="txtr" name="txtr" value="<?=$vpjq?>"><br>

        <label for="ni">total pizza hawaiana</label>
        <input type="txt" id="txtr" name="txtr" value="<?=$vph?>"><br>

        <label for="ni">total pizza pepperoni </label>
        <input type="txt" id="txtr" name="txtr" value="<?=$vpp?>"><br>

        <label for="ni">total pizza jamon  </label>
        <input type="txt" id="txtr" name="txtr" value="<?=$vpqp?>"><br>

        <label for="ni">venta del dia </label>
        <input type="txt" id="txtr" name="txtr" value="<?=$total?>"><br>
       
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">
        </div>
    </form>
</body>
</html>