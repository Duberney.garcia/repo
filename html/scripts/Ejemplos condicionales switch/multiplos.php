<?php
    //determinar si un numero es multiplos de 3 y 5 
    //variables
    $n=0; $r="";
    if(isset($_POST["btncalcular"])){
        //entrada
        $n=$_POST["txtn"];

        //proceso
        if($n %3==0 && $n %5==0){
            $r="es multiplo de 3 y 5 ";
        }else{
            $r="no es multiplo de 3 o 5";
        }
    }
?>
<html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Multiplo de 3 y 5</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="multiplos.php">
        <label for="number">Numero</label>   
        <input type="number" id="txtn" name="txtn" value="<?=$n?>">
        &nbsp;
        <label for="resultado">Resultado: </label>
        <input type="txt" id="txtr" name="txtr" value="<?=$r?>">
        <div>
            <input type="submit" name="btncalcular" id="btncalcular" value="calcular">
        </div>
    </form>
</body>
</html>