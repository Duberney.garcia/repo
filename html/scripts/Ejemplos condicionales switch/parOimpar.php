<?php
    //determinar si es un numero es par o impar
    $n=0; $r="";
    if(isset($_POST["btncalcular"])){
        //enttrada
        $n=$_POST["txtn"];
        //proceso
        if($n%2==0){
            $r="ES UN NUMERO PAR";
        }else{
            $r="ES IMPAR";
        }
    }    
?>
<html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Par o impar</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="parOimpar.php">
        <label for="number">Numero</label>   
        <input type="number" id="txtn" name="txtn" value="<?=$n?>">
        &nbsp;
        <label for="resultado">Resultado: </label>
        <input type="txt" id="txtr" name="txtr" value="<?=$r?>">
        <div>
            <input type="submit" name="btncalcular" id="btncalcular" value="calcular">
        </div>
    </form>
</body>
</html>