<?php
    /* Dados 5 numeros obtener la suma */
    $suma=0; $i=0;
    //Arreglo
    $n=0;
    $n=array();
    if(isset($_POST["btncalcular"])){
        //Entrada
        $n[0]=$_POST["txtn1"];
        $n[1]=$_POST["txtn2"];
        $n[2]=$_POST["txtn3"];
        $n[3]=$_POST["txtn4"];
        $n[4]=$_POST["txtn5"];

        //proceso
        for ($i=0; $i <=4 ; $i++) { 
            $suma += $n[$i];
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Problema suma 5 numneros</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <form action="sumaArray.php" method="post">
        <h1>Suma 5 numeros</h1>
        <label for="number1">Numero 1</label>
        <input type="number" name="txtn1" id="txtn1" value="<?=$n[0]?>" size ="10"><hr>
        <label for="number2">Numero 2</label>
        <input type="number" name="txtn2" id="txtn2" value="<?=$n[1]?>" size ="10"><hr>
        <label for="number3">Numero 3</label>
        <input type="number" name="txtn3" id="txtn3" value="<?=$n[2]?>" size ="10"><hr>
        <label for="number4">Numero 4</label>
        <input type="number" name="txtn4" id="txtn4" value="<?=$n[3]?>" size ="10"><hr>
        <label for="number5">Numero 5</label>
        <input type="number" name="txtn5" id="txtn5" value="<?=$n[4]?>" size ="10"><hr>

        <label for="suma">Suma</label>
        <input type="text" name="txts" id="txts" value="<?=$suma?>"  size="10">
        &nbsp;
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">

    </form>
</body>
</html>