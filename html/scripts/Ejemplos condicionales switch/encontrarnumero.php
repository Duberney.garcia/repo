<?php
    /* Busque un numero en 5 numeros ingresados, determinela posicion y si eciste o no el numero buscado,
        use el metodo de busqueda secuencial */
    
    $nb=0; $p=0; $i=0;
    $r=""; $n=0;
    
    
    //Arreglo    
    $n=array();
    if(isset($_POST["btncalcular"])){
        
        //Entrada
        $n[0]=$_POST["txtn1"];
        $n[1]=$_POST["txtn2"];
        $n[2]=$_POST["txtn3"];
        $n[3]=$_POST["txtn4"];
        $n[4]=$_POST["txtn5"];
        $nb=$_POST["txtnb"];
        //proceso
        $r="NO EXISTE";
        $p=-1;
        for ($i=0; $i <=count($n) -1; $i++) { 
            if ($n[$i]==$nb) {
                $r="EXISTE";
                $p=$i;
                break;
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Encontrar numero en array</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <form action="encontrarnumero.php" method="post">
        <h1>Encontrar numero</h1>
        <label for="number1">Numero 1</label>
        <input type="number" name="txtn1" id="txtn1" value="<?=$n[0]?>" size ="10"><hr>
        <label for="number2">Numero 2</label>
        <input type="number" name="txtn2" id="txtn2" value="<?=$n[1]?>" size ="10"><hr>
        <label for="number3">Numero 3</label>
        <input type="number" name="txtn3" id="txtn3" value="<?=$n[2]?>" size ="10"><hr>
        <label for="number4">Numero 4</label>
        <input type="number" name="txtn4" id="txtn4" value="<?=$n[3]?>" size ="10"><hr>
        <label for="number5">Numero 5</label>
        <input type="number" name="txtn5" id="txtn5" value="<?=$n[4]?>" size ="10"><hr>

        <label for="numerobuscuado">Numero a buscar</label>
        <input type="text" name="txtnb" id="txtnb" value="<?=$nb?>"  size="10">
        <label for="Respuesta">Respuesta</label>
        <input type="text" name="txtr" id="txtr" value="<?=$r?>"  size="10">
        <label for="position">Posicion</label>
        <input type="text" name="txtp" id="txtp" value="<?=$p?>"  size="10">

        &nbsp;
        <input type="submit" name="btncalcular" id="btncalcular" value="Calcular">

    </form>
</body>
</html>