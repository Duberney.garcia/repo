<?php
/*
Dado un caracter, determinar si es vocal
*/ 

// variables
$c=""; 
$r="";
if(isset($_POST["btncalcular"])){
    //entrada
    $c=$_POST["txtc"];
    $r="No es vocal";   
    if($c=="a" || $c=="A"){
        $r="Es vocal";
    }else if($c=="e" || $c=="E"){
        $r="Es vocal";
    }else if($c=="i" || $c=="I"){
        $r="Es vocal";
    }else if($c=="o" || $c=="O"){
        $r="Es vocal";
    }else if($c=="u" || $c=="U"){
        $r="Es vocal";
    }
}
?>
<html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Problema vocales</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="vocales.php">
        <h1>Ejercicio vocales</h1>
        <div>
            <label for="c">Caracter</label>
            <input type="text" id="txtc" name="txtc" value="<?=$c?>">
            &nbsp;
            <label for="resultado">Resultado</label>
            <input type="text" id="txtr" value="<?=$r?>">
            &nbsp;
            <input type="submit" name="btncalcular" id="btncalcular" value="calcular">
        </div>
    </form>
</body>
</html>