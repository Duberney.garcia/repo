<?php
/*
dado un numero numero entero ded un digito(1-9)
devolverlo en letras
*/

//VARIABLES

$num=0; 
$res="";
if(isset($_POST["btncalcular"])){
    //entrada
    $num=(float)$_POST["txtn"];
    
    switch($num){
        case 0:
        $res= "cero";
        break;
        case 1:
        $res= "uno";
        break;
        case 2:
        $res= "dos";
        break;
        case 3:
        $res= "tres";
        break;
        case 4:
        $res= "cuatro";
        break;
        case 5:
        $res= "cinco";
        break;
        case 6:
        $res= "seis";
        break;
        case 7:
        $res= "siete";
        break;
        case 8:
        $res= "ocho";
        break;
        case 9:
        $res= "nueve";
        break;
        
        default;
        $res= "numero invalido";
    }       
 }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>numeros</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form  method="post" action="numero1al9.php">
    <h1> numeros</h1>
    <div>
        <label for="number">numero</label>
        <input type="number" id="txtn" name="txtn" value="<?=$num?>">
        &nbsp;
        <label for="resultado">resultado</label>
        <input type="text" id="txtres" name="txtres" value="<?=$res?>">
        &nbsp;
        <input type="submit" name="btncalcular" id="btncalcular" value="calcular">
       
    </div>
    </form>
</body>
</html>

