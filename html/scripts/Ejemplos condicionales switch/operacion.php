<?php
/* Dados los numeros enteros y un operador (*,+,-,/)
devolver el resultado de la operacion, considere que si el segundo 
numero es 0 y el operador es / no es divisible con primer numero, 
devolver el resultado 0
*/
    $op="";
    $num1=0; $num2=0; $res=0;
    if(isset($_POST["btncalcular"])){
        $op=$_POST["txtop"];
        $num1=(int)$_POST["txtn1"];
        $num2=(int)$_POST["txtn2"];

        //proceso
        switch ($op) {
            case "+" :
                $res=$num1+$num2;
                break;
            case "*" :
                $res=$num1*$num2;
                break;                        
            case "-" :
                $res=$num1-$num2;
                break;
            case "/" :
                if ($num2 !=0) {
                    $res=$num1/$num2;
                }else{
                    $res;
                }   
                break;                        
        }
    }
?>
<html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Par o impar</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <form method="post" action="operacion.php">
        <label for="number">Numero1</label>   
        <input type="number" id="txtn1" name="txtn1" value="<?=$num1?>">
        <label for="number">Numero2</label>   
        <input type="number" id="txtn2" name="txtn2" value="<?=$num2?>">
        <label for="operacion">Operacion</label>                   
        <select name="txtop" id="txtop" value="<?=$op?>">
            <option value=""></option>
            <option value="+">+</option>
            <option value="*">*</option>
            <option value="/">/</option>
            <option value="-">-</option>
        </select>
        &nbsp;
        <label for="resultado">Resultado: </label>
        <input type="txt" id="txtr" name="txtr" value="<?=$res?>">
        <div>
            <input type="submit" name="btncalcular" id="btncalcular" value="calcular">
        </div>
    </form>
</body>
</html>