<?php
    /* oredene 4 numeros usando el metodo de ordenacion por intercambio (burbuja) 
       ANALISIS:
       para la solucion de este problema se requiere que el usuario ingrese 4 numeros; 
       luego que el sistema devuelva los numero ordenados
    */
    
    $tmp=0; $i=0; $j=0;
    $lt=0; $ls=0;
    
    
    //Arreglo
    $n=0;    
    $n=array();
   
    if(isset($_POST["btncalcular"])){
        
        //Entrada
        $n[0]=$_POST["txtn1"];
        $n[1]=$_POST["txtn2"];
        $n[2]=$_POST["txtn3"];
        $n[3]=$_POST["txtn4"];

        //proceso
        $lt=0;
        $ls=count($n)-1;

        for ($i=$lt; $i <=$ls -1; $i++) {
            for ($j=$lt; $j <=$ls -1; $j++) {
                if ($n[$j] > $n[$j +1]) {
                    $tmp = $n[$j];
                    $n[$j] = $n[$j+1];
                    $n[$j +1] = $tmp;
                }
            }    
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>metodo burbuja</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <form action="arrayburbuja.php" method="post">
        <h1>Ordenameinto por metodo burbuja</h1>
        <label for="number1">Numero 1</label>
        <input type="number" name="txtn1" id="txtn1" value="<?=$n[0]?>" size ="5"><hr>
        <label for="number2">Numero 2</label>
        <input type="number" name="txtn2" id="txtn2" value="<?=$n[1]?>" size ="5"><hr>
        <label for="number3">Numero 3</label>
        <input type="number" name="txtn3" id="txtn3" value="<?=$n[2]?>" size ="5"><hr>
        <label for="number4">Numero 4</label>
        <input type="number" name="txtn4" id="txtn4" value="<?=$n[3]?>" size ="5"><hr>        
        &nbsp;
        <input type="submit" name="btncalcular" id="btncalcular" value="Ordenar">

    </form>
</body>
</html>